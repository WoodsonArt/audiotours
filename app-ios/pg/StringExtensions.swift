import Foundation
import UIKit
import MediaPlayer

extension String
{
    var html2AttStr:NSAttributedString
    {
        let contents: NSMutableAttributedString?
            
            
            
        do {
            let attrTextStyle = NSMutableParagraphStyle()
            attrTextStyle.alignment = NSTextAlignment.Center
            contents = try NSMutableAttributedString(data: dataUsingEncoding(NSUTF8StringEncoding)!, options:[NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding], documentAttributes: nil)
        } catch _ {
            contents = nil
        }
        
        
        
        return NSAttributedString(attributedString: contents!)
    }
}