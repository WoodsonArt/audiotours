import Foundation
import UIKit

class ViewControllerTourGallery : UICollectionViewController
{
    var jsonFileToLoad:String = String()
    private let reuseIdentifier = "GalleryCell"
    private let cellSize = 135
    private let cellSizePad = 300
    private let sectionInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
    private let sectionInsetsPad = UIEdgeInsets(top: 32.0, left: 32.0, bottom: 32.0, right: 32.0)
    private var artwork:Array<TourArtwork> = []
    private var selectedIndex:Int = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loadTourDataFromJson()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        if (segue.identifier == "ShowArtworkDetail")
        {
            let artworkViewController:ViewControllerArtwork = segue.destinationViewController as! ViewControllerArtwork
            artworkViewController.artworkTitle = artwork[selectedIndex].artworkDetailviewTitle
            artworkViewController.artworkCaption = artwork[selectedIndex].artworkCaption
            artworkViewController.artworkImagePath = artwork[selectedIndex].artworkImage
            artworkViewController.artworkVideoList = artwork[selectedIndex].artworkVideos
        }
    }
    
    override func shouldAutorotate() -> Bool
    {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask
    {
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad
        {
            return UIInterfaceOrientationMask.AllButUpsideDown
        }
        else
        {
            return [UIInterfaceOrientationMask.Portrait, UIInterfaceOrientationMask.PortraitUpsideDown]
        }
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return artwork.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ViewControllerTourGalleryCell
        cell.imageView.image = UIImage(named: artwork[indexPath.row].artworkThumbnail)
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        selectedIndex = indexPath.row
        self.performSegueWithIdentifier("ShowArtworkDetail", sender: self)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if (UIDevice.currentDevice().userInterfaceIdiom == .Phone) {
            return CGSize(width: cellSize, height: cellSize)
        }
        else {
            return CGSize(width: cellSizePad, height: cellSizePad)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad
        {
            return sectionInsetsPad
        }
        else
        {
            return sectionInsets
        }
    }
    
    func loadTourDataFromJson()
    {
        var hasLoadedMetaFromFirstIndex = false
        let path = NSBundle.mainBundle().pathForResource(jsonFileToLoad, ofType: "")

        let text: String?
        do {
            text = try String(contentsOfFile: path!, encoding: NSUTF8StringEncoding)
        } catch _ {
            text = nil
        }
        
        
        
        let rootJsonArray: AnyObject?
        do {
            rootJsonArray = try NSJSONSerialization.JSONObjectWithData(text!.dataUsingEncoding(NSUTF8StringEncoding)!, options: NSJSONReadingOptions(rawValue: 0))
        } catch _ {
            rootJsonArray = nil
        }

        
        
        self.artwork = []
        if rootJsonArray is Array<AnyObject>
        {
            for json in rootJsonArray as! Array<AnyObject>
            {
                if(hasLoadedMetaFromFirstIndex)
                {
                    let foundArtworkTableTitle:String = json["Artwork Tableview Title"] as! String
                    let foundArtworkDetailViewTitle:String = json["Artwork Detail Title"] as! String
                    let foundArtworkThumbnail:String = json["Artwork Thumbnail"] as! String
                    let foundArtworkImage:String = json["Artwork Image"] as! String
                    let foundArtworkCaption:String = json["Artwork Caption"] as! String
                    var foundArtworkVideos:[TourArtworkVideo] = []
                    if let videos = json["Artwork Videos"] as? NSArray
                    {
                        for video in videos
                        {
                            foundArtworkVideos.append(TourArtworkVideo(videoTitle: video["Video Title"] as! String, videoFilePath: video["Video File"] as! String))
                        }
                    }
                    self.artwork.append(TourArtwork(artworkTitle: foundArtworkTableTitle, artworkDetailTitle: foundArtworkDetailViewTitle, artworkThumbnail: foundArtworkThumbnail, artworkImage: foundArtworkImage, artworkCaption: foundArtworkCaption, artworkVideos: foundArtworkVideos))
                }
                else
                {
                    self.navigationItem.title = json["Exhibition Title"] as? String
                    hasLoadedMetaFromFirstIndex = true
                }
            }
        }
    }
}