import Foundation
import UIKit
import QuartzCore

class CustomNavController: UINavigationController
{
    static let defaultTransition:CATransition = CATransition()
    static let defaultTransitionDuration = 0.15
   
    override init(rootViewController: UIViewController)
    {
        super.init(rootViewController: rootViewController)
        CustomNavController.defaultTransition.duration = CustomNavController.defaultTransitionDuration
        CustomNavController.defaultTransition.type = kCATransitionFade
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        CustomNavController.defaultTransition.duration = CustomNavController.defaultTransitionDuration
        CustomNavController.defaultTransition.type = kCATransitionFade
    }
    
    override func popViewControllerAnimated(animated: Bool) -> UIViewController?
    {
        self.view.layer.addAnimation(CustomNavController.defaultTransition, forKey: "kCATransition")
        return super.popViewControllerAnimated(false)
    }
    
    override func pushViewController(viewController: UIViewController, animated: Bool)
    {
        self.view.layer.addAnimation(CustomNavController.defaultTransition, forKey: "kCATransition")
        return super.pushViewController(viewController, animated: false)
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask
    {
        return topViewController!.supportedInterfaceOrientations()
    }
    
    override func shouldAutorotate() -> Bool
    {
       return topViewController!.shouldAutorotate()
    }
}