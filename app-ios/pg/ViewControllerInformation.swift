import UIKit
import MapKit

class ViewControllerInformation: UIViewController, MKMapViewDelegate
{
    @IBOutlet weak var mapView: MKMapView?
    
    override func viewDidLoad()
    {
        if let informationMap = mapView
        {
            informationMap.delegate = self
            informationMap.setRegion(MKCoordinateRegionMakeWithDistance(woodsonCoordinates, 2000, 2000), animated: true)
            informationMap.addAnnotation(woodsonCoordinatesPin)
            informationMap.selectAnnotation(woodsonCoordinatesPin, animated: true)
        }
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
    }
    
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    
    override func shouldAutorotate() -> Bool
    {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask
    {
        return [UIInterfaceOrientationMask.Portrait, UIInterfaceOrientationMask.PortraitUpsideDown]
    }
    
    @IBAction func tapCategoryVisit(recognizer: UITapGestureRecognizer)
    {
        self.performSegueWithIdentifier("SegueTapVisit", sender: self)
    }
    
    @IBAction func tapCategoryLearn(recognizer: UITapGestureRecognizer)
    {
        self.performSegueWithIdentifier("SegueTapLearnMore", sender: self)
    }
    
    @IBAction func tapCategoryLocation(recognizer: UITapGestureRecognizer)
    {
        self.performSegueWithIdentifier("SegueTapLocation", sender: self)
    }
    
    @IBAction func tapCategoryHours(recognizer: UITapGestureRecognizer)
    {
        self.performSegueWithIdentifier("SegueTapHours", sender: self)
    }

    @IBAction func tapVideoDirectorWelcome(sender: UIButton)
    {
        videoController.playVideo(self, navn: "DirectorsWelcome", type: "mp4")
    }
    
    @IBAction func tapVideoOurStory(sender: UIButton)
    {
        videoController.playVideo(self, navn: "OurStory", type: "mp4")
    }
}