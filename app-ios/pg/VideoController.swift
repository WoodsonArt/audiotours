import Foundation
import UIKit
import MediaPlayer

class VideoControllerView : MPMoviePlayerViewController
{
    override func shouldAutorotate() -> Bool
    {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask
    {
        return UIInterfaceOrientationMask.AllButUpsideDown
    }
    
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
}


class VideoController : NSObject
{
    func playVideo(view:UIViewController, navn:String, type:String)
    {
        let path = NSBundle.mainBundle().pathForResource(navn, ofType: type)
        let url = NSURL.fileURLWithPath(path!)
        
        let playerVC:VideoControllerView = VideoControllerView(contentURL: url)
        
        playerVC.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
        
        view.presentMoviePlayerViewControllerAnimated(playerVC)
        playerVC.moviePlayer.prepareToPlay()
        playerVC.moviePlayer.play()
    }
}