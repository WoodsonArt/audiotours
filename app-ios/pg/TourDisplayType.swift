import Foundation

enum TourDisplayType
{
    case TableView
    case GridView
    case None
}