import Foundation
import UIKit

class ViewControllerTour: UITableViewController
{
     var selectedArtworkPointer:Int = 0
     var jsonFileToLoad:String = ""
     var tourSetData:Array<TourArtwork> = []
    
    override  func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        loadTourData()
        tableView.separatorInset = UIEdgeInsetsZero
        tableView.layoutMargins = UIEdgeInsetsZero
    }
    
    override  func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override  func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override  func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    
    override  func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        if (segue.identifier == "ShowArtworkDetailFromTable")
        {
            let artworkViewController:ViewControllerArtwork = segue.destinationViewController as! ViewControllerArtwork
            artworkViewController.artworkTitle = tourSetData[selectedArtworkPointer].artworkDetailviewTitle
            artworkViewController.artworkCaption = tourSetData[selectedArtworkPointer].artworkCaption
            artworkViewController.artworkImagePath = tourSetData[selectedArtworkPointer].artworkImage
            artworkViewController.artworkVideoList = tourSetData[selectedArtworkPointer].artworkVideos
        }
    }
    
    override func shouldAutorotate() -> Bool
    {
        return true;
    }

    
    override  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 88
    }
    
    override  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tourSetData.count
    }
    
    override  func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
    
    override  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let tableViewCell:ViewControllerTourCell = self.tableView.dequeueReusableCellWithIdentifier("Cell") as! ViewControllerTourCell
        let title = "<span style=\"font-size: 17px;font-weight:lighter;font-family:Avenir-Book;\">" + tourSetData[indexPath.row].artworkTableviewTitle + "</span>"
        tableViewCell.cellLabel.attributedText = title.html2AttStr
        
        
        tableViewCell.cellImage.image = UIImage(named: tourSetData[indexPath.row].artworkThumbnail)
        tableViewCell.layoutMargins = UIEdgeInsetsZero
        return tableViewCell
    }
    
    override  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        selectedArtworkPointer = indexPath.row
        self.performSegueWithIdentifier("ShowArtworkDetailFromTable", sender: self)
    }
    
     func loadTourData()
    {
        var hasLoadedMetaFromFirstIndex = false
        let path = NSBundle.mainBundle().pathForResource(jsonFileToLoad, ofType: "")
        print(jsonFileToLoad)
        
        let text: String?
        do {
            text = try String(contentsOfFile: path!, encoding: NSUTF8StringEncoding)
        } catch _ {
            text = nil
        }

        let rootJsonArray: AnyObject?
        do {
            rootJsonArray = try NSJSONSerialization.JSONObjectWithData(text!.dataUsingEncoding(NSUTF8StringEncoding)!, options: NSJSONReadingOptions(rawValue: 0))
        } catch _ {
            rootJsonArray = nil
        }

        
        self.tourSetData = []
        if rootJsonArray is Array<AnyObject>
        {
            for json in rootJsonArray as! Array<AnyObject>
            {
                if(hasLoadedMetaFromFirstIndex)
                {
                    let foundArtworkTableTitle:String = json["Artwork Tableview Title"] as! String
                    let foundArtworkDetailViewTitle:String = json["Artwork Detail Title"] as! String
                    let foundArtworkThumbnail:String = json["Artwork Thumbnail"] as! String
                    let foundArtworkImage:String = json["Artwork Image"] as! String
                    let foundArtworkCaption:String = json["Artwork Caption"] as! String
                    var foundArtworkVideos:[TourArtworkVideo] = []
                    if let videos = json["Artwork Videos"] as? NSArray
                    {
                        for video in videos
                        {
                            foundArtworkVideos.append(TourArtworkVideo(videoTitle: video["Video Title"] as! String, videoFilePath: video["Video File"] as! String))
                        }
                    }
                    tourSetData.append(TourArtwork(artworkTitle: foundArtworkTableTitle, artworkDetailTitle: foundArtworkDetailViewTitle, artworkThumbnail: foundArtworkThumbnail, artworkImage: foundArtworkImage, artworkCaption: foundArtworkCaption, artworkVideos: foundArtworkVideos))
                }
                else
                {
                    self.navigationItem.title = json["Exhibition Title"] as? String
                    hasLoadedMetaFromFirstIndex = true
                }
            }
        }
    }
}