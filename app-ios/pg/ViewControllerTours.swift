import Foundation
import UIKit

class ViewControllerTours: UITableViewController
{
    var tourSetPointer = 0
    var tourSetData:Array<Tour> = []
    var tourSetSelectedLink:String = String()
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        addBackButton()
        loadTourData()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        if (segue.identifier == "ExhibitionView")
        {
            let tourViewController:ViewControllerTour = segue.destinationViewController as! ViewControllerTour
            tourViewController.jsonFileToLoad = tourSetSelectedLink
        }
        else if(segue.identifier == "ExhibitionViewGrid")
        {
            let tourViewController:ViewControllerTourGallery = segue.destinationViewController as! ViewControllerTourGallery
            tourViewController.jsonFileToLoad = tourSetSelectedLink
        }
    }
    
    override func shouldAutorotate() -> Bool
    {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask
    {
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad
        {
            return UIInterfaceOrientationMask.AllButUpsideDown
        }
        else
        {
            return [UIInterfaceOrientationMask.Portrait, UIInterfaceOrientationMask.PortraitUpsideDown]
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return tableView.frame.height / 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tourSetData.count
    }
    
    override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return false
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let tableViewCell:ViewControllerToursCell = self.tableView.dequeueReusableCellWithIdentifier("Cell") as! ViewControllerToursCell
        let tableViewCellBackgroundImage = UIImage(named: tourSetData[indexPath.row].tourCoverPhoto)
        let tableViewCellBackgroundImageView = UIImageView(image: tableViewCellBackgroundImage)
        tableViewCellBackgroundImageView.clipsToBounds = true
        tableViewCellBackgroundImageView.contentMode = UIViewContentMode.ScaleAspectFill
        tableViewCell.backgroundView = tableViewCellBackgroundImageView
        tableViewCell.button.setTitle(tourSetData[indexPath.row].tourBeginButtonText, forState: UIControlState.Normal)
        tableViewCell.button.addTarget(self, action: "tourButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        tableViewCell.button.tag = indexPath.row
        return tableViewCell
    }
    
    func isNumeric(a: String) -> Bool {
        return Int(a) != nil
    }
    
    func tourButtonClicked(sender: UIButton)
    {
        let indexPathRow = sender.tag
        let indexPathRowLink = tourSetData[indexPathRow].tourBeginButtonLink
        if isNumeric(indexPathRowLink)
        {
            let tourSelectorViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TourSelector") as? ViewControllerTours
            tourSelectorViewController!.tourSetPointer = Int(indexPathRowLink)!
            self.navigationController?.pushViewController(tourSelectorViewController!, animated: true)
        }
        else
        {
            self.tourSetSelectedLink = indexPathRowLink
            if tourSetData[indexPathRow].tourDisplayType == TourDisplayType.TableView
            {
                self.performSegueWithIdentifier("ExhibitionView", sender: self)
            }
            else if tourSetData[indexPathRow].tourDisplayType == TourDisplayType.GridView
            {
                self.performSegueWithIdentifier("ExhibitionViewGrid", sender: self)
            }
            else
            {
                print("Invalid display type for segue")
            }
        }
    }
    
    func backButtonClicked(sender: UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func loadTourData()
    {
        let path = NSBundle.mainBundle().pathForResource("Tours", ofType: "json")
  
        let text: String?
        do {
            text = try String(contentsOfFile: path!, encoding: NSUTF8StringEncoding)
        } catch _ {
            text = nil
        }
        
        
        let rootJsonArray: AnyObject?
        do {
            rootJsonArray = try NSJSONSerialization.JSONObjectWithData(text!.dataUsingEncoding(NSUTF8StringEncoding)!, options: NSJSONReadingOptions(rawValue: 0))
        } catch _ {
            rootJsonArray = nil
        }
        
        self.tourSetData = []
        if rootJsonArray is Array<AnyObject>
        {
            for json in rootJsonArray as! Array<AnyObject>
            {
                if (json["id"] as! Int) == tourSetPointer
                {
                    if let tours = json["content"] as? NSArray
                    {
                        for tour in tours
                        {
                            let image:String = tour["image"] as! String
                            let link:String = tour["link"] as! String
                            let button:String = tour["button"] as! String
                            let displayType:String = tour["Display Type"] as! String
                            if displayType == "Grid"
                            {
                                tourSetData.append(Tour(tourCoverPhoto: image, tourBeginButtonText: button, tourBeginButtonLink: link, tourDisplayType: TourDisplayType.GridView))
                            }
                            else if displayType == "Table"
                            {
                                tourSetData.append(Tour(tourCoverPhoto: image, tourBeginButtonText: button, tourBeginButtonLink: link, tourDisplayType: TourDisplayType.TableView))
                            }
                            else
                            {
                                tourSetData.append(Tour(tourCoverPhoto: image, tourBeginButtonText: button, tourBeginButtonLink: link, tourDisplayType: TourDisplayType.None))
                            }
                        }
                    }
                }
            }
        }
    }
    
    func addBackButton()
    {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 65, height: 35))
        button.setTitle("Back", forState: UIControlState.Normal)
        button.setBackgroundImage(UIImage(named: "Button.png"), forState: UIControlState.Normal)
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.alpha = 0.75
        button.titleLabel?.font = UIFont(name: "Avenir-Book", size: 17)
        button.addTarget(self, action: "backButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
        self.view.bringSubviewToFront(button)
    }
}