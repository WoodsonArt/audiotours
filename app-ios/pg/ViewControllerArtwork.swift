import Foundation
import UIKit
import AVKit
import AVFoundation

class ViewControllerArtwork: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var artworkImageView:UIImageView!
    @IBOutlet var artworkCaptionView:UILabel!
    @IBOutlet var artworkVideoTable:UITableView!
    var artworkTitle:String = String()
    var artworkImagePath:String = String()
    var artworkCaption:String = String()
    var artworkVideoList:[TourArtworkVideo] = []
    var selectedIndex = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        artworkVideoTable.tableFooterView = UIView(frame: CGRectZero)
        artworkVideoTable.dataSource = self;
        artworkVideoTable.delegate = self;
        artworkVideoTable.separatorInset = UIEdgeInsetsZero
        artworkVideoTable.layoutMargins = UIEdgeInsetsZero
    }
    
    override func viewWillDisappear(animated: Bool) {
        
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationItem.title = artworkTitle
        
        
        let title = "<center><span style=\"font-size: 17px;font-weight:lighter;font-family:Avenir-Book;\">" + artworkCaption + "</span></center>"
        artworkImageView.image = UIImage(named: artworkImagePath)
        
        artworkCaptionView.attributedText = title.html2AttStr
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle
    {
        return UIStatusBarStyle.LightContent
    }
    
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
    }
    
    override func shouldAutorotate() -> Bool
    {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask
    {
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad
        {
            return UIInterfaceOrientationMask.AllButUpsideDown
        }
        else
        {
            return [UIInterfaceOrientationMask.Portrait, UIInterfaceOrientationMask.PortraitUpsideDown]
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return artworkVideoList.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let view:UIView = UIView()
        view.backgroundColor = UIColor.blackColor()
        return view
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let tableViewCell:UITableViewCell? = self.artworkVideoTable.dequeueReusableCellWithIdentifier("Cell")
        tableViewCell!.layoutMargins = UIEdgeInsetsZero
        tableViewCell?.textLabel?.text = artworkVideoList[indexPath.row].videoTitle
        return tableViewCell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        selectedIndex = indexPath.row
        videoController.playVideo(self, navn: artworkVideoList[selectedIndex].videoFilePath, type: String())
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}