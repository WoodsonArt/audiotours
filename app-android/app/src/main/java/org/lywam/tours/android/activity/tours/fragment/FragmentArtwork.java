package org.lywam.tours.android.activity.tours.fragment;

import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.WoodsonArt;
import org.lywam.tours.android.activity.tours.adapter.AdapterListArtworkVideo;
import org.lywam.tours.android.data.ExhibitionArtwork;
import org.lywam.tours.phonegap.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

/**
 * @author Austin Walhof
 */
public class FragmentArtwork extends Fragment implements OnItemClickListener
{
    private ExhibitionArtwork artwork;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(WoodsonArt.Tag.EXHIBITION_ARTWORK.name()))
        {
            this.artwork = (new Gson()).fromJson(getArguments().getString(WoodsonArt.Tag.EXHIBITION_ARTWORK.name()), ExhibitionArtwork.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.activity_exhibition_artwork_fragment, container, false);

        ImageView artworkImage = (ImageView) rootView.findViewById(R.id.artDetailImage);
        artworkImage.setImageDrawable(AndroidPlatformUtils.getInstance().getDrawableFromObb(getActivity(), artwork.getImagePath(), false));

        TextView artworkCaption = (TextView) rootView.findViewById(R.id.captionView);
        artworkCaption.setText(AndroidPlatformUtils.getInstance().applyCaptionFormatting(artwork.getCaption()));

        ListView videoList = (ListView) rootView.findViewById(R.id.artworkDetailVideoList);
        videoList.setAdapter(new AdapterListArtworkVideo(getActivity(), artwork.getVideos()));
        videoList.setOnItemClickListener(this);

        AndroidPlatformUtils.getInstance().overrideTextViewFonts(getActivity(), rootView);

        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        AndroidPlatformUtils.getInstance().playExpansionVideo(this.getActivity(), artwork.getVideos().get(position).getVideoPath());
    }
}