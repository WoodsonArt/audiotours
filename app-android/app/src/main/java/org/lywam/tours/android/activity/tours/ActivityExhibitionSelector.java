package org.lywam.tours.android.activity.tours;

import org.json.JSONException;
import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.WoodsonArt;
import org.lywam.tours.android.activity.ActivityWoodsonArt;
import org.lywam.tours.android.activity.tours.adapter.AdapterListPage;
import org.lywam.tours.android.data.Page;
import org.lywam.tours.phonegap.R;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

/**
 * @author Austin Walhof
 */
public class ActivityExhibitionSelector extends ActivityWoodsonArt implements OnItemClickListener
{
    private Page tour;

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.activity_page);

        AndroidPlatformUtils.getInstance().setupPossibleScreenOrientations(this, false);
        AndroidPlatformUtils.getInstance().overrideTextViewFonts(this, this.getWindow().getDecorView());

        Bundle extras = this.getIntent().getExtras();
        if(extras == null)
        {
            throw new RuntimeException("No page data was provided to the activity.");
        }
        else
        {
            String jsonText = AndroidPlatformUtils.getInstance().getJsonFromObb(this, "Tours.json");
            try
            {
                tour = getAppInstance().getJsonParser().getSelectorPage(extras.getInt(WoodsonArt.Tag.PAGE.name()), jsonText);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        ListView pageEntryListView = (ListView) this.findViewById(R.id.listView1);
        pageEntryListView.setAdapter(new AdapterListPage(this, tour));
        pageEntryListView.setOnItemClickListener(this);

        Button backButton = (Button) this.findViewById(R.id.tourButtonBack);
        backButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                finish();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        return;
    }
}