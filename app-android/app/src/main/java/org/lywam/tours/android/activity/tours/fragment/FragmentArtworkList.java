package org.lywam.tours.android.activity.tours.fragment;

import org.json.JSONException;
import org.lywam.tours.android.WoodsonArt;
import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.activity.tours.adapter.AdapterListArtwork;
import org.lywam.tours.android.activity.tours.fragment.callback.CallbackArtworkList;
import org.lywam.tours.android.data.Exhibition;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

/**
 * @author Austin Walhof
 */
public class FragmentArtworkList extends ListFragment
{
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    private Exhibition exhibition;
    private CallbackArtworkList mCallbacks = CallbackArtworkList.dummy;
    private int mActivatedPosition = ListView.INVALID_POSITION;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle extras = this.getActivity().getIntent().getExtras();
        if (extras != null)
        {
            String source = AndroidPlatformUtils.getInstance().getJsonFromObb(this.getActivity(), extras.getString(WoodsonArt.Tag.EXHIBITION.name()));
            try
            {
                this.exhibition = ((WoodsonArt) getActivity().getApplication()).getJsonParser().createExhibition(source);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        AdapterListArtwork listAdapter = new AdapterListArtwork(this.getActivity(), this.exhibition);
        this.setListAdapter(listAdapter);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION))
        {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (!(activity instanceof CallbackArtworkList))
        {
            throw new IllegalStateException("This activity must implement the required callback.");
        }
        mCallbacks = (CallbackArtworkList) activity;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mCallbacks = CallbackArtworkList.dummy;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id)
    {
        super.onListItemClick(listView, view, position, id);
        mCallbacks.onItemSelected(exhibition.getArtworksAsArray()[position]);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION)
        {
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    public void setActivateOnItemClick(boolean activateOnItemClick)
    {
        getListView().setChoiceMode(activateOnItemClick ? ListView.CHOICE_MODE_SINGLE : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position)
    {
        getListView().setItemChecked(position, true);
        if (position == ListView.INVALID_POSITION)
        {
            getListView().setItemChecked(mActivatedPosition, false);
        }
        mActivatedPosition = position;
    }
}