package org.lywam.tours.android.data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Austin Walhof
 */
public class ExhibitionArtwork
{
    private String artworkTitle;
    private String artworkTitleTable;
    private String imagePath;
    private String imagePathThumbnail;
    private String caption;
    private final List<ExhibitionArtworkVideo> videos = new ArrayList<ExhibitionArtworkVideo>();

    public String getArtworkTitle()
    {
        return artworkTitle;
    }

    public void setArtworkTitle(String artworkTitle)
    {
        this.artworkTitle = artworkTitle;
    }

    public String getArtworkTitleTable()
    {
        return artworkTitleTable;
    }

    public void setArtworkTitleTable(String artworkTitleTable)
    {
        this.artworkTitleTable = artworkTitleTable;
    }

    public String getImagePath()
    {
        return imagePath;
    }

    public void setImagePath(String imagePath)
    {
        this.imagePath = imagePath;
    }

    public String getImagePathThumbnail()
    {
        return imagePathThumbnail;
    }

    public void setImagePathThumbnail(String imagePathThumbnail)
    {
        this.imagePathThumbnail = imagePathThumbnail;
    }

    public String getCaption()
    {
        return caption;
    }

    public void setCaption(String caption)
    {
        this.caption = caption;
    }

    public List<ExhibitionArtworkVideo> getVideos()
    {
        return videos;
    }
}