package org.lywam.tours.android.data.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lywam.tours.android.data.Exhibition;
import org.lywam.tours.android.data.ExhibitionArtwork;
import org.lywam.tours.android.data.ExhibitionArtworkVideo;
import org.lywam.tours.android.data.Page;
import org.lywam.tours.android.data.PageEntry;

/**
 * @author Austin Walhof
 */
public class JsonParser
{
	public Exhibition createExhibition(String in) throws JSONException
	{
        Exhibition exhibition = null;
        JSONArray reader = new JSONArray(in);
        for(int index = 0; index < reader.length(); index++)
        {
            JSONObject item = reader.getJSONObject(index);
            if(index == 0)
            {
                String title = item.getString("Exhibition Title");
                exhibition = new Exhibition(title);
                continue;
            }
            ExhibitionArtwork artwork = new ExhibitionArtwork();
            artwork.setArtworkTitle(item.getString("Artwork Detail Title"));
            artwork.setArtworkTitleTable(item.getString("Artwork Tableview Title"));
            artwork.setImagePath(item.getString("Artwork Image"));
            artwork.setImagePathThumbnail(item.getString("Artwork Thumbnail"));
            artwork.setCaption(item.getString("Artwork Caption"));
            JSONArray videoArray = item.getJSONArray("Artwork Videos");
            for(int videoIndex = 0; videoIndex < videoArray.length(); videoIndex++)
            {
                JSONObject videoItem = videoArray.getJSONObject(videoIndex);
                String videoTitle = videoItem.getString("Video Title");
                String videoPath = videoItem.getString("Video File");
                ExhibitionArtworkVideo video = new ExhibitionArtworkVideo(videoPath, videoTitle);
                artwork.getVideos().add(video);
            }
            exhibition.getArtworks().add(artwork);
        }
        return exhibition;
	}

	public Page getSelectorPage(int pageId, String in) throws JSONException
	{
		JSONArray reader = new JSONArray(in);
		for(int index = 0; index < reader.length(); index++)
		{
			JSONObject page = reader.getJSONObject(index);
            JSONArray pageEntries = page.getJSONArray("content");
            int xid = page.getInt("id");
            if(pageId == xid)
            {
                Page selectorPage = new Page(xid);
                for(int contentIndex = 0; contentIndex < pageEntries.length(); contentIndex++)
                {
                    JSONObject entryItem = pageEntries.getJSONObject(contentIndex);
                    String imagePath = entryItem.getString("image");
                    String buttonLink = entryItem.getString("link");
                    String buttonText = entryItem.getString("button");
                    PageEntry entry = new PageEntry(imagePath, buttonLink, buttonText);
                    selectorPage.getPageEntries().add(entry);
                }
                return selectorPage;
            }
        }
        return null;
	}
}