package org.lywam.tours.android.activity.media;

import android.app.Activity;
import android.content.Context;
import android.view.KeyEvent;
import android.widget.MediaController;

/**
 * @author Austin Walhof
 */
public class ActivityVideoPlayerControls extends MediaController
{
    public ActivityVideoPlayerControls(Context context)
    {
        super(context);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK)
        {
            Activity activityContext = (Activity) getContext();
            activityContext.finish();
            return true;
        }
        else
        {
            return super.dispatchKeyEvent(event);
        }
    }
}