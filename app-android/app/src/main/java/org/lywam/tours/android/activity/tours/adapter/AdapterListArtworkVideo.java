package org.lywam.tours.android.activity.tours.adapter;

import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.data.ExhibitionArtworkVideo;
import org.lywam.tours.phonegap.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * @author Austin Walhof
 */
public class AdapterListArtworkVideo extends BaseAdapter
{
    private final List<ExhibitionArtworkVideo> videos;
    private final LayoutInflater inflater;

    public AdapterListArtworkVideo(Context context, List<ExhibitionArtworkVideo> videos)
    {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.videos = videos;
    }

    @Override
    public int getCount()
    {
        return videos.size();
    }

    @Override
    public Object getItem(int position)
    {
        return videos.get(position);
    }

    @Override
    public long getItemId(int id)
    {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.activity_exhibition_artwork_cell, parent, false);
        }

        AndroidPlatformUtils.getInstance().overrideTextViewFonts(convertView.getContext(), convertView);
        TextView text = (TextView) convertView.findViewById(R.id.infoCellText);
        text.setText(videos.get(position).getVideoTitle());

        return convertView;
    }
}