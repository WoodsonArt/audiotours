package org.lywam.tours.android.activity;

import org.lywam.tours.android.WoodsonArt;
import org.lywam.tours.android.service.ExpansionDownloadServices;
import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.activity.information.ActivityInformation;
import org.lywam.tours.android.activity.tours.ActivityExhibitionSelector;
import org.lywam.tours.phonegap.R;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Messenger;
import android.view.View;
import android.widget.Button;
import com.google.android.vending.expansion.downloader.DownloadProgressInfo;
import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.google.android.vending.expansion.downloader.DownloaderServiceMarshaller;
import com.google.android.vending.expansion.downloader.IDownloaderClient;
import com.google.android.vending.expansion.downloader.IDownloaderService;
import com.google.android.vending.expansion.downloader.IStub;

/**
 * @author Austin Walhof
 */
public class ActivityHome extends ActivityWoodsonArt implements IDownloaderClient
{
    private ProgressDialog progressDialog;
    private IStub downloadClientStub;
    private IDownloaderService remoteService;

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.activity_home);
        AndroidPlatformUtils.getInstance().setupPossibleScreenOrientations(this, false);
        AndroidPlatformUtils.getInstance().overrideTextViewFonts(this, this.getWindow().getDecorView());
        this.performApplicationContentUpdates();
        this.addButtonActionListeners();
    }

    @Override
    public void onServiceConnected(Messenger messenger)
    {
        this.remoteService = DownloaderServiceMarshaller.CreateProxy(messenger);
        this.remoteService.onClientUpdated(downloadClientStub.getMessenger());
    }

    @Override
    public void onDownloadProgress(DownloadProgressInfo downloadProgressInformation)
    {
    }

    @Override
    public void onDownloadStateChanged(int state)
    {
        if(state == IDownloaderClient.STATE_COMPLETED)
        {
            this.showSecondaryDialog(this.getStringResource(R.string.expansion_generic_title_completed), this.getStringResource(R.string.expansion_generic_message_finished), this.getStringResource(R.string.expansion_dialog_button_finish));
        }
        else if(state == IDownloaderClient.STATE_FAILED || state == IDownloaderClient.STATE_FAILED_CANCELED || state == IDownloaderClient.STATE_FAILED_FETCHING_URL || state ==  IDownloaderClient.STATE_FAILED_UNLICENSED)
        {
            this.showSecondaryDialog(this.getStringResource(R.string.expansion_generic_title_failed), this.getStringResource(R.string.expansion_generic_message_resources_failed), this.getStringResource(R.string.expansion_dialog_button_retry));
        }
        else if(state == IDownloaderClient.STATE_FAILED_SDCARD_FULL)
        {
            this.showSecondaryDialog(this.getStringResource(R.string.expansion_generic_title_failed), this.getStringResource(R.string.expansion_generic_message_no_space), this.getStringResource(R.string.expansion_dialog_button_retry));
        }
        else if(state == IDownloaderClient.STATE_PAUSED_NEED_CELLULAR_PERMISSION || state == IDownloaderClient.STATE_PAUSED_NEED_WIFI || state == IDownloaderClient.STATE_PAUSED_NETWORK_SETUP_FAILURE || state == IDownloaderClient.STATE_PAUSED_NETWORK_UNAVAILABLE || state == IDownloaderClient.STATE_PAUSED_ROAMING || state == IDownloaderClient.STATE_PAUSED_WIFI_DISABLED || state == IDownloaderClient.STATE_PAUSED_WIFI_DISABLED_NEED_CELLULAR_PERMISSION)
        {
            this.progressDialog.setTitle(R.string.expansion_generic_title_paused);
            this.progressDialog.setMessage(this.getStringResource(R.string.expansion_generic_message_network_error));
        }
        else if(state == IDownloaderClient.STATE_PAUSED_SDCARD_UNAVAILABLE)
        {
            this.progressDialog.setTitle(R.string.expansion_generic_title_paused);
            this.progressDialog.setMessage(this.getStringResource(R.string.expansion_generic_message_no_storage_device));
        }
        else if(state == IDownloaderClient.STATE_DOWNLOADING)
        {
            this.progressDialog.setTitle(this.getStringResource(R.string.expansion_generic_title_downloading));
            this.progressDialog.setMessage(this.getStringResource(R.string.expansion_generic_message_downloading));
        }
    }

    private CharSequence getStringResource(int id)
    {
        return this.getApplicationContext().getText(id);
    }

    private Intent getHomeIntent()
    {
        Intent parentIntent = getIntent();
        Intent homeScreenIntent = new Intent(this, getClass());
        homeScreenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        homeScreenIntent.setAction(parentIntent.getAction());
        if (parentIntent.getCategories() != null)
        {
            for (String cat : parentIntent.getCategories())
            {
                homeScreenIntent.addCategory(cat);
            }
        }
        return homeScreenIntent;
    }

    private void performApplicationContentUpdates()
    {
        if (!AndroidPlatformUtils.getInstance().doesExpansionExist(this.getApplicationContext()))
        {
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, getHomeIntent(), PendingIntent.FLAG_UPDATE_CURRENT);
            int updateCheckResult = AndroidPlatformUtils.getInstance().startDownloadServiceIfRequired(getApplicationContext(), pendingIntent, ExpansionDownloadServices.class);
            if (DownloaderClientMarshaller.NO_DOWNLOAD_REQUIRED != updateCheckResult)
            {
                this.downloadClientStub = DownloaderClientMarshaller.CreateStub(this, ExpansionDownloadServices.class);
                this.downloadClientStub.connect(getApplicationContext());
                this.progressDialog = ProgressDialog.show(this, getApplicationContext().getString(R.string.expansion_generic_title_downloading), getApplicationContext().getString(R.string.expansion_generic_message_downloading), true);
                this.progressDialog.setIndeterminate(true);
                this.progressDialog.setCancelable(false);
            }
        }
    }

    private void showSecondaryDialog(CharSequence title, CharSequence message, CharSequence buttonText)
    {
        progressDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(message).setCancelable(false).setPositiveButton(buttonText, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int id)
            {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), ActivityHome.class);
                startActivity(intent);
            }
        });
        builder.create().show();
    }

    private void addButtonActionListeners()
    {
        this.findViewById(R.id.buttonTours).setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View view)
            {
                Intent intent = new Intent(getApplicationContext(), ActivityExhibitionSelector.class);
                intent.putExtra(WoodsonArt.Tag.PAGE.name(), 0);
                startActivity(intent);
            }
        });
        this.findViewById(R.id.buttonInformation).setOnClickListener(new Button.OnClickListener()
        {
            public void onClick(View view)
            {
                startActivity(new Intent(getApplicationContext(), ActivityInformation.class));
            }
        });
    }
}