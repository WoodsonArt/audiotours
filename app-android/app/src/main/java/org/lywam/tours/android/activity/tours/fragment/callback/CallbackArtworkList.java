package org.lywam.tours.android.activity.tours.fragment.callback;

import android.util.Log;

import org.lywam.tours.android.WoodsonArt;
import org.lywam.tours.android.data.ExhibitionArtwork;

/**
 * @author Austin Walhof
 */
public interface CallbackArtworkList
{
    void onItemSelected(ExhibitionArtwork detail);

    CallbackArtworkList dummy = new CallbackArtworkList()
    {
        @Override
        public void onItemSelected(ExhibitionArtwork detail)
        {
            Log.v(WoodsonArt.class.getName(), "No callback was available to handle the item select event.");
        }
    };
}