package org.lywam.tours.android.activity.tours;

import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.WoodsonArt;
import org.lywam.tours.android.activity.ActivityWoodsonArt;
import org.lywam.tours.android.activity.tours.fragment.FragmentArtwork;
import org.lywam.tours.android.data.ExhibitionArtwork;
import org.lywam.tours.phonegap.R;

import android.os.Bundle;

import com.google.gson.Gson;

/**
 * @author Austin Walhof
 */
public class ActivityArtwork extends ActivityWoodsonArt
{
    @Override
    protected void onCreate(Bundle savedBundleInstance)
    {
        super.onCreate(savedBundleInstance);
        setContentView(R.layout.activity_exhibition_artwork);

        setupToolBar();
        AndroidPlatformUtils.getInstance().setupPossibleScreenOrientations(this, false);
        AndroidPlatformUtils.getInstance().overrideTextViewFonts(this, this.getWindow().getDecorView());

        setupFragment(savedBundleInstance);
    }

    private void setupFragment(Bundle savedBundleInstance)
    {
        if (savedBundleInstance == null)
        {
            ExhibitionArtwork artworkData = (new Gson()).fromJson(getIntent().getStringExtra(WoodsonArt.Tag.EXHIBITION_ARTWORK.name()), ExhibitionArtwork.class);
            getSupportActionBar().setTitle(artworkData.getArtworkTitle());

            Bundle fragmentArguments = new Bundle();
            fragmentArguments.putString(WoodsonArt.Tag.EXHIBITION_ARTWORK.name(), (new Gson()).toJson(artworkData));

            FragmentArtwork fragmentArtwork = new FragmentArtwork();
            fragmentArtwork.setArguments(fragmentArguments);

            getSupportFragmentManager().beginTransaction().add(R.id.item_detail_container, fragmentArtwork).commit();
        }
    }
}