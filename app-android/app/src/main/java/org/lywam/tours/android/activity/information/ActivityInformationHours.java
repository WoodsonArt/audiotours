package org.lywam.tours.android.activity.information;

import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.activity.ActivityWoodsonArt;
import org.lywam.tours.phonegap.R;

import android.os.Bundle;

/**
 * @author Austin Walhof
 */
public class ActivityInformationHours extends ActivityWoodsonArt
{
    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.activity_information_hours);

        setupToolBar();
        AndroidPlatformUtils.getInstance().setupPossibleScreenOrientations(this, false);
        AndroidPlatformUtils.getInstance().overrideTextViewFonts(this, this.getWindow().getDecorView());
    }
}