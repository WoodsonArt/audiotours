package org.lywam.tours.android.data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Austin Walhof
 */
public class Page
{
    private final int id;
    private final List<PageEntry> pageEntries = new ArrayList<PageEntry>();

    public Page(int id)
    {
        this.id = id;
    }

    public List<PageEntry> getPageEntries()
    {
        return pageEntries;
    }

    public int getId()
    {
        return id;
    }
}