package org.lywam.tours.android.service;

import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.content.pm.PackageManager.NameNotFoundException;

        import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;

/**
 * @author Austin Walhof
 */
public class ExpansionDownloadReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        try
        {
            DownloaderClientMarshaller.startDownloadServiceIfRequired(context, intent, ExpansionDownloadServices.class);
        }
        catch (NameNotFoundException e)
        {
            e.printStackTrace();
        }
    }
}