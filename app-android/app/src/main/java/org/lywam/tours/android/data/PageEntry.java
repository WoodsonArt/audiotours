package org.lywam.tours.android.data;

/**
 * @author Austin Walhof
 */
public class PageEntry
{
    private final String imagePath;
    private final String buttonLink;
    private final String buttonText;

    public PageEntry(String imagePath, String buttonLink, String buttonText)
    {
        this.imagePath = imagePath;
        this.buttonLink = buttonLink;
        this.buttonText = buttonText;
    }

    public String getButtonText()
    {
        return buttonText;
    }

    public String getImagePath()
    {
        return imagePath;
    }

    public String getButtonLink()
    {
        return buttonLink;
    }
}