package org.lywam.tours.android.data;

/**
 * @author Austin Walhof
 */
public class ExhibitionArtworkVideo
{
    private final String videoPath, videoTitle;

    public ExhibitionArtworkVideo(String videoPath, String videoTitle)
    {
        this.videoPath = videoPath;
        this.videoTitle = videoTitle;
    }

    public String getVideoPath()
    {
        return videoPath;
    }

    public String getVideoTitle()
    {
        return videoTitle;
    }
}