package org.lywam.tours.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.lywam.tours.android.activity.media.ActivityVideoPlayer;
import org.lywam.tours.phonegap.R;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.google.android.vending.expansion.downloader.Helpers;
import com.google.android.vending.expansion.downloader.impl.DownloaderService;

/**
 * @author Austin Walhof
 */
public class AndroidPlatformUtils
{
    private static AndroidPlatformUtils instance = new AndroidPlatformUtils();

    public int getApplicationVersion(Context applicationContext) throws NameNotFoundException
    {
        return applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
    }

    public String getExpansionFileName(Context applicationContext)
    {
        try
        {
            return Helpers.getExpansionAPKFileName(applicationContext, true, getApplicationVersion(applicationContext));
        }
        catch (NameNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public boolean doesExpansionExist(Context applicationContext)
    {
        return new File(Helpers.generateSaveFileName(applicationContext, getExpansionFileName(applicationContext))).exists();
    }

    public ZipResourceFile getExpansionFile(Activity activity)
    {
        if (doesExpansionExist(activity))
        {
            try
            {
                return APKExpansionSupport.getAPKExpansionZipFile(activity.getBaseContext(), getApplicationVersion(activity), getApplicationVersion(activity));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return null;
    }

    public int startDownloadServiceIfRequired(Context context, PendingIntent notificationClient, Class<?> serviceClass)
    {
        try
        {
            return DownloaderService.startDownloadServiceIfRequired(context, notificationClient, serviceClass);
        }
        catch (NameNotFoundException e)
        {
            e.printStackTrace();
        }
        return -1;
    }

    public String getJsonFromObb(Activity activity, String filePath)
    {
        try
        {
            InputStream inputStream = getExpansionFile(activity).getInputStream(filePath);
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            inputStream.close();
            return new String(buffer);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public String getVideoFromObb(Activity activity, String videoPath)
    {
        int offset = 0, read = 0;
        try
        {
            InputStream imageStream = getExpansionFile(activity).getInputStream(videoPath);
            byte[] frameData = new byte[imageStream.available()];
            while ((offset < frameData.length) && (read = imageStream.read(frameData, offset, frameData.length - offset)) >= 0)
            {
                offset += read;
            }
            imageStream.close();
            File streamingCacheFile = File.createTempFile("streamingCache", null, activity.getCacheDir());
            FileOutputStream fileWriter = new FileOutputStream(new File(streamingCacheFile.getAbsolutePath()));
            fileWriter.write(frameData);
            fileWriter.close();
            return streamingCacheFile.getAbsolutePath();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public Drawable getDrawableFromObb(Activity activity, String filePath, boolean rounded)
    {
        try
        {
            final InputStream inputStream = getExpansionFile(activity).getInputStream(filePath);
            Bitmap  drawable = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            if(rounded)
            {
                RoundedBitmapDrawable roundedDrawable = RoundedBitmapDrawableFactory.create(activity.getResources(), drawable);
                roundedDrawable.setCornerRadius(32);
                return roundedDrawable;
            }
            return new BitmapDrawable(drawable);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public void playExpansionVideo(Activity activity, String videoPath)
    {
        Intent intent = new Intent(activity.getApplicationContext(), ActivityVideoPlayer.class);
        intent.putExtra(WoodsonArt.Tag.EXHIBITION_ARTWORK_VIDEO.name(), videoPath);
        activity.startActivity(intent);
    }

    public Typeface getDefaultFont(AssetManager assets)
    {
        return Typeface.createFromAsset(assets, "font/default.ttf");
    }

    public void setupPossibleScreenOrientations(final Activity activity, final boolean force)
    {
        if (activity.getResources().getBoolean(R.bool.portrait_only) || force)
        {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    public int getDipValueOfPixels(Context context, int pixelsToConvert)
    {
        return (int) (pixelsToConvert * context.getResources().getDisplayMetrics().density + 0.5f);
    }

    public int getStatusBarHeight(Activity activity)
    {
        return activity.getResources().getDimensionPixelSize(activity.getResources().getIdentifier("status_bar_height", "dimen", "android"));
    }

    public Point getDisplaySize(Activity activity)
    {
        Point displaySize = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(displaySize);
        return displaySize;
    }

    public int getWindowHeight(Activity activity)
    {
        return getDisplaySize(activity).y - getStatusBarHeight(activity);
    }

    public int measureText(CharSequence charSequence, Typeface typeface, float textSize)
    {
        Paint textPainter = new Paint();
        textPainter.setTypeface(typeface);
        textPainter.setTextSize(textSize);
        return (int) textPainter.measureText(charSequence, 0, charSequence.length());
    }

    public int getRequiredButtonWidth(TextView textView, Context context)
    {
        return measureText(textView.getText(), getDefaultFont(context.getAssets()), textView.getTextSize()) + getDipValueOfPixels(context, 40);
    }

    public void overrideTextViewFonts(final Context context, final View view)
    {
        try
        {
            if (view instanceof ViewGroup)
            {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                {
                    View child = viewGroup.getChildAt(i);
                    overrideTextViewFonts(context, child);
                }
            }
            else if (view instanceof TextView)
            {
                ((TextView) view).setTypeface(this.getDefaultFont(context.getAssets()));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public Spanned applyCaptionFormatting(String stringToFormat)
    {
        StringBuilder htmlSource = new StringBuilder(stringToFormat);
        Matcher regexMatcher = Pattern.compile(Pattern.quote("/") + "(.*?)" + Pattern.quote("/")).matcher(htmlSource.toString());
        while (regexMatcher.find())
        {
            StringBuilder subPattern = new StringBuilder(regexMatcher.group(0));
            subPattern.deleteCharAt(0);
            subPattern.deleteCharAt(subPattern.length() - 1);
            for (int i = 0; i < htmlSource.toString().length(); i++)
            {
                if (htmlSource.toString().charAt(i) == '/')
                {
                    if (i == 0 || !(htmlSource.toString().charAt(i - 1) == '\\'))
                    {
                        htmlSource.deleteCharAt(i);
                    }
                }
            }
            for (int j = 0; j < subPattern.toString().length(); j++)
            {
                if (subPattern.toString().charAt(j) == '\\')
                {
                    subPattern.deleteCharAt(j);
                }
            }
            for (int k = 0; k < htmlSource.toString().length(); k++)
            {
                if (htmlSource.toString().charAt(k) == '\\' && htmlSource.toString().charAt(k + 1) == '/')
                {
                    htmlSource.deleteCharAt(k);
                }
            }
            htmlSource.insert(htmlSource.toString().indexOf(subPattern.toString()), "<i>");
            htmlSource.insert(htmlSource.toString().indexOf(subPattern.toString()) + subPattern.toString().length() + 1, "<//i>"); // The closing italics tag MUST be escaped!
        }
        return Html.fromHtml(htmlSource.toString());
    }

    public Spanned getTableViewCaptionText(String title)
    {
        return Html.fromHtml(title);
    }

    public boolean isInteger(String stringToParse)
    {
        try
        {
            Integer.parseInt(stringToParse);
            return true;
        }
        catch (NumberFormatException ignored)
        {
            return false;
        }
    }

    public static AndroidPlatformUtils getInstance()
    {
        return instance;
    }
}