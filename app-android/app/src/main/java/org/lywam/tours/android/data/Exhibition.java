package org.lywam.tours.android.data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Austin Walhof
 */
public class Exhibition
{
    private final List<ExhibitionArtwork> artworks = new ArrayList<ExhibitionArtwork>();
    private final String exhibitionTitle;

    public Exhibition(String exhibitionTitle)
    {
        this.exhibitionTitle = exhibitionTitle;
    }

    public String getExhibitionTitle()
    {
        return exhibitionTitle;
    }

    public List<ExhibitionArtwork> getArtworks()
    {
        return artworks;
    }

    public ExhibitionArtwork[] getArtworksAsArray()
    {
        return getArtworks().toArray(new ExhibitionArtwork[artworks.size()]);
    }
}