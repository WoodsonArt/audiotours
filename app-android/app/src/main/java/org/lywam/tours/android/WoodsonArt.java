package org.lywam.tours.android;

import org.lywam.tours.android.data.json.JsonParser;
import org.lywam.tours.phonegap.BuildConfig;

import android.app.Application;
import android.os.StrictMode;
import android.util.Log;

/**
 * @author Austin Walhof
 */
public class WoodsonArt extends Application
{
    private final JsonParser parserInstance = new JsonParser();

    @Override
    public void onCreate()
    {
        super.onCreate();
        if (BuildConfig.DEBUG)
        {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
        }
    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
        Log.e(WoodsonArt.class.getName(), "Received a low memory notice.");
    }

    public JsonParser getJsonParser()
    {
        return parserInstance;
    }

    public enum Tag
    {
        PAGE, EXHIBITION_ARTWORK, EXHIBITION, EXHIBITION_ARTWORK_VIDEO;
    }
}