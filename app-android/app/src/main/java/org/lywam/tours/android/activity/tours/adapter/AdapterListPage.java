package org.lywam.tours.android.activity.tours.adapter;

import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.WoodsonArt;
import org.lywam.tours.android.activity.tours.ActivityArtworkList;
import org.lywam.tours.android.activity.tours.ActivityExhibitionSelector;
import org.lywam.tours.android.data.Page;
import org.lywam.tours.android.data.PageEntry;
import org.lywam.tours.android.activity.tours.adapter.model.AdapterListPageModel;
import org.lywam.tours.phonegap.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * @author Austin Walhof
 */
public class AdapterListPage extends BaseAdapter
{
    private LayoutInflater inflater;
    private Activity activity;
    private Page page;

    public AdapterListPage(Activity activity, Page page)
    {
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        this.page = page;
    }

    @Override
    public int getCount()
    {
        return page.getPageEntries().size();
    }

    @Override
    public Object getItem(int pos)
    {
        return page.getPageEntries().get(pos);
    }

    @Override
    public long getItemId(int id)
    {
        return id;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent)
    {
        final AdapterListPageModel model;
        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.activity_page_cell, parent, false);
            model = new AdapterListPageModel();
            model.layout = (RelativeLayout) convertView.findViewById(R.id.relativeLayout1);
            model.coverImage = (ImageView) convertView.findViewById(R.id.tourImg);
            model.link = (Button) convertView.findViewById(R.id.tourButton);
            model.pageData = (PageEntry) this.getItem(position);
            convertView.setTag(model);
        }
        else
        {
            model = (AdapterListPageModel) convertView.getTag();
        }

        Drawable entryImage = AndroidPlatformUtils.getInstance().getDrawableFromObb(this.activity, model.pageData.getImagePath(), false);

        model.layout.getLayoutParams().height = AndroidPlatformUtils.getInstance().getWindowHeight(activity) / this.getCount() + 1;
        model.coverImage.setImageDrawable(entryImage);
        model.link.setText(model.pageData.getButtonText());
        model.link.getLayoutParams().width = AndroidPlatformUtils.getInstance().getRequiredButtonWidth(model.link, this.activity);
        model.link.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent;
                if (AndroidPlatformUtils.getInstance().isInteger(model.pageData.getButtonLink()))
                {
                    intent = new Intent(activity.getApplicationContext(), ActivityExhibitionSelector.class);
                    intent.putExtra(WoodsonArt.Tag.PAGE.name(), Integer.valueOf(model.pageData.getButtonLink()));
                } else
                {
                    intent = new Intent(activity.getApplicationContext(), ActivityArtworkList.class);
                    intent.putExtra(WoodsonArt.Tag.EXHIBITION.name(), model.pageData.getButtonLink());
                }
                activity.startActivity(intent);
            }
        });

        AndroidPlatformUtils.getInstance().overrideTextViewFonts(this.activity, model.link);
        return convertView;
    }
}