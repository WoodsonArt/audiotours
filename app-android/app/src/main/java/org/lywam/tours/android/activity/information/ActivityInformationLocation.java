package org.lywam.tours.android.activity.information;

import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.activity.ActivityWoodsonArt;
import org.lywam.tours.phonegap.R;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * @author Austin Walhof
 */
public class ActivityInformationLocation extends ActivityWoodsonArt
{
    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.activity_information_location);

        setupToolBar();
        AndroidPlatformUtils.getInstance().setupPossibleScreenOrientations(this, false);
        AndroidPlatformUtils.getInstance().overrideTextViewFonts(this, this.getWindow().getDecorView());

        WebView webview = (WebView) findViewById(R.id.webView1);
        webview.setWebViewClient(new WebViewClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl("http://maps.google.com/maps?" + "ll=44.9620,-89.6130&z=20");
    }
}