package org.lywam.tours.android.service;

import com.google.android.vending.expansion.downloader.impl.DownloaderService;

/**
 * @author Austin Walhof
 */
public class ExpansionDownloadServices extends DownloaderService
{
    @Override
    public String getPublicKey()
    {
        return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlzD2ChgVvlSp2HVKLoL8qWrv7NSPkdAM+SH8mw0HdTd74Bl9yeTfrkj2KcNtB1RMI6IsfwMuvgokLHoIbSd+nDRji4zGea0pkhEoYub8JCkB1WBzHc5xNhSqs9itiAgeNRethF+Y3+AVxYQJD3jLCdFKgOc62ctN9feUTDrsfMDR/tfQiV1aMY2ahiPjMinsc3tDmWcnkyevzseINaxJkFZDBepzSY1wx8bRlq7QHf78VjegekwkpK0rUegoHNZ+Ud4hV+wcr36WoCXYAvXMibdFCBqbD8PYz6xXxNUVGVP9qvMTRQ05FvpxeHXLJQKYn3UWYXwYv7y30+8YV4HW1QIDAQAB";
    }

    @Override
    public byte[] getSALT()
    {
        return new byte[] { -63, 117, -55, 46, 17, 34, 55, 63, -16, 63, 25, 13, 16, -22, 72, 2, 11, 43, 37, -60 };
    }

    @Override
    public String getAlarmReceiverClassName()
    {
        return ExpansionDownloadReceiver.class.getName();
    }
}