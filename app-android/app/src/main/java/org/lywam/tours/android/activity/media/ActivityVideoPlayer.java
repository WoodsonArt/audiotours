package org.lywam.tours.android.activity.media;

import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.WoodsonArt;
import org.lywam.tours.android.activity.ActivityWoodsonArt;
import org.lywam.tours.phonegap.R;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.VideoView;

/**
 * @author Austin Walhof
 */
public class ActivityVideoPlayer extends ActivityWoodsonArt
{
    private VideoView videoPlayerView;
    private ActivityVideoPlayerControls defaultMediaController;

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.activity_video_player);

        AndroidPlatformUtils.getInstance().overrideTextViewFonts(this, getWindow().getDecorView());

        this.videoPlayerView = (VideoView) findViewById(R.id.videoView1);
        this.defaultMediaController = new ActivityVideoPlayerControls(this);
        if (getIntent().getExtras() != null)
        {
            String videoAsset = getIntent().getExtras().getString(WoodsonArt.Tag.EXHIBITION_ARTWORK_VIDEO.name());
            playVideoFromExpansion(videoAsset);
        }
    }

    public void playVideoFromExpansion(String file)
    {
        defaultMediaController.setMediaPlayer(videoPlayerView);
        videoPlayerView.setVideoPath(AndroidPlatformUtils.getInstance().getVideoFromObb(this, file));
        videoPlayerView.setMediaController(defaultMediaController);
        videoPlayerView.requestFocus();
        videoPlayerView.start();
        videoPlayerView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                videoPlayerView.stopPlayback();
                finish();
            }
        });
    }
}