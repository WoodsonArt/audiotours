package org.lywam.tours.android.activity.information;

import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.activity.ActivityWoodsonArt;
import org.lywam.tours.phonegap.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * @author Austin Walhof
 */
public class ActivityInformation extends ActivityWoodsonArt
{
    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.activity_information);

        setupToolBar();
        AndroidPlatformUtils.getInstance().setupPossibleScreenOrientations(this, true);
        AndroidPlatformUtils.getInstance().overrideTextViewFonts(this, this.getWindow().getDecorView());

        CardView cardVisit = (CardView) this.findViewById(R.id.card_view);
        CardView cardLearn = (CardView) this.findViewById(R.id.card_view2);
        CardView cardHours = (CardView) this.findViewById(R.id.card_view3);
        CardView cardGmaps = (CardView) this.findViewById(R.id.card_view4);

        cardVisit.setOnClickListener(clickListener);
        cardLearn.setOnClickListener(clickListener);
        cardHours.setOnClickListener(clickListener);
        cardGmaps.setOnClickListener(clickListener);
    }

    private final OnClickListener clickListener = new OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            switch(view.getId())
            {
                case R.id.card_view:
                    startActivity(new Intent(getApplicationContext(), ActivityInformationVisit.class));
                    break;
                case R.id.card_view2:
                    startActivity(new Intent(getApplicationContext(), ActivityInformationLearnMore.class));
                    break;
                case R.id.card_view3:
                    startActivity(new Intent(getApplicationContext(), ActivityInformationHours.class));
                    break;
                case R.id.card_view4:
                    startActivity(new Intent(getApplicationContext(), ActivityInformationLocation.class));
                    break;
            }
        }
    };
}