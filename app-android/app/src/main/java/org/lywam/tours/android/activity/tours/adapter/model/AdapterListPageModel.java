package org.lywam.tours.android.activity.tours.adapter.model;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.lywam.tours.android.data.PageEntry;

/**
 * @author Austin Walhof
 */
public class AdapterListPageModel
{
    public PageEntry pageData;
    public RelativeLayout layout;
    public ImageView coverImage;
    public Button link;
}