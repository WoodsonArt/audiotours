package org.lywam.tours.android.activity.tours;

import org.json.JSONException;
import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.WoodsonArt;
import org.lywam.tours.android.activity.ActivityWoodsonArt;
import org.lywam.tours.android.activity.tours.fragment.FragmentArtwork;
import org.lywam.tours.android.activity.tours.fragment.FragmentArtworkList;
import org.lywam.tours.android.activity.tours.fragment.callback.CallbackArtworkList;
import org.lywam.tours.android.data.Exhibition;
import org.lywam.tours.android.data.ExhibitionArtwork;
import org.lywam.tours.phonegap.R;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.google.gson.Gson;

/**
 * @author Austin Walhof
 */
public class ActivityArtworkList extends ActivityWoodsonArt implements CallbackArtworkList
{
    private boolean noFragmentSupport;

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.activity_exhibition_list);

        setupToolBar();
        AndroidPlatformUtils.getInstance().setupPossibleScreenOrientations(this, false);
        AndroidPlatformUtils.getInstance().overrideTextViewFonts(this, this.getWindow().getDecorView());

        this.noFragmentSupport = findViewById(R.id.item_detail_container) == null;
        ((FragmentArtworkList) getSupportFragmentManager().findFragmentById(R.id.item_list)).setActivateOnItemClick(true);

        if(this.getIntent().getExtras() == null)
        {
            throw new RuntimeException("No exhibition data was provided to the activity.");
        }
        else
        {
            ListView listView = ((FragmentArtworkList) getSupportFragmentManager().findFragmentById(R.id.item_list)).getListView();
            listView.setDivider(this.getResources().getDrawable(android.R.color.black));
            listView.setDividerHeight(1);
            listView.setFooterDividersEnabled(false);
            try
            {
                String json = AndroidPlatformUtils.getInstance().getJsonFromObb(this, this.getIntent().getExtras().getString(WoodsonArt.Tag.EXHIBITION.name()));
                Exhibition exhibition = getAppInstance().getJsonParser().createExhibition(json);
                this.getSupportActionBar().setTitle(exhibition.getExhibitionTitle());
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemSelected(ExhibitionArtwork detail)
    {
        String serializedDataArtworkDetail = (new Gson()).toJson(detail);
        if (noFragmentSupport)
        {
            Intent detailIntent = new Intent(this, ActivityArtwork.class);
            detailIntent.putExtra(WoodsonArt.Tag.EXHIBITION_ARTWORK.name(), serializedDataArtworkDetail);
            startActivity(detailIntent);
        }
        else
        {
            Bundle argumentBundle = new Bundle();
            FragmentArtwork detailFragment = new FragmentArtwork();
            argumentBundle.putString(WoodsonArt.Tag.EXHIBITION_ARTWORK.name(), serializedDataArtworkDetail);
            detailFragment.setArguments(argumentBundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.item_detail_container, detailFragment).commit();
        }
    }
}