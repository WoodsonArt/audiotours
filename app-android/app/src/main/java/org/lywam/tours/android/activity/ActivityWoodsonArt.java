package org.lywam.tours.android.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.lywam.tours.android.WoodsonArt;
import org.lywam.tours.phonegap.R;

/**
 * @author Austin Walhof
 */
public abstract class ActivityWoodsonArt extends AppCompatActivity
{
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        this.overridePendingTransition(org.lywam.tours.phonegap.R.anim.anim_fade, org.lywam.tours.phonegap.R.anim.anim_fade_out);
    }

    @Override
    public void finish()
    {
        super.finish();
        this.overridePendingTransition(org.lywam.tours.phonegap.R.anim.anim_fade, org.lywam.tours.phonegap.R.anim.anim_fade_out);
    }

    public WoodsonArt getAppInstance()
    {
        return (WoodsonArt) getApplication();
    }

    public void setupToolBar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}