package org.lywam.tours.android.activity.tours.adapter;

import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.data.Exhibition;
import org.lywam.tours.phonegap.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Austin Walhof
 */
public class AdapterListArtwork extends BaseAdapter
{
    private final Activity activity;
    private final Exhibition exhibition;
    private final LayoutInflater inflater;

    public AdapterListArtwork(Activity activity, Exhibition exhibition)
    {
        this.activity = activity;
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.exhibition = exhibition;
    }

    @Override
    public int getCount()
    {
        return exhibition.getArtworks().size();
    }

    @Override
    public Object getItem(int position)
    {
        return exhibition.getArtworks().get(position);
    }

    @Override
    public long getItemId(int id)
    {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.activity_exhibition_list_cell, null);
        }

        TextView artworkTitle = (TextView) convertView.findViewById(R.id.infoCellText);
        artworkTitle.setTypeface(AndroidPlatformUtils.getInstance().getDefaultFont(activity.getAssets()));
        artworkTitle.setText(AndroidPlatformUtils.getInstance().getTableViewCaptionText(exhibition.getArtworksAsArray()[position].getArtworkTitleTable()));

        ImageView artworkImage = (ImageView) convertView.findViewById(R.id.detImage);
        artworkImage.setImageDrawable(AndroidPlatformUtils.getInstance().getDrawableFromObb(activity, exhibition.getArtworksAsArray()[position].getImagePathThumbnail(), true));

        return convertView;
    }
}