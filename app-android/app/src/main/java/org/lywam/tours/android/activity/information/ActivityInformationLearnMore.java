package org.lywam.tours.android.activity.information;

import org.lywam.tours.android.AndroidPlatformUtils;
import org.lywam.tours.android.activity.ActivityWoodsonArt;
import org.lywam.tours.phonegap.R;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * @author Austin Walhof
 */
public class ActivityInformationLearnMore extends ActivityWoodsonArt
{
    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.activity_information_learnmore);

        setupToolBar();
        AndroidPlatformUtils.getInstance().setupPossibleScreenOrientations(this, false);
        AndroidPlatformUtils.getInstance().overrideTextViewFonts(this, this.getWindow().getDecorView());

        Button directorsWelcome = (Button) this.findViewById(R.id.dirWelcome);
        Button listenOurStories = (Button) this.findViewById(R.id.ourStory);

        directorsWelcome.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                playVideo("DirectorsWelcome.mp4");
            }
        });

        listenOurStories.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                playVideo("OurStory.mp4");
            }
        });
    }

    private void playVideo(String fileName)
    {
        AndroidPlatformUtils.getInstance().playExpansionVideo(this, fileName);
    }
}