package org.lywam.tours.ziptool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class ExpansionCreator
{
	private int versionCode;
	private File root;
	private File tempDirectory;
	private File outputFile;

	public ExpansionCreator(String[] args)
	{
		if (args.length > 2 || args.length < 2)
		{
			throw new RuntimeException("An incorrect amount of arguments was supplied to the program.");
		}
		else
		{
			try
			{
				this.root = new File(args[0]);
				this.versionCode = Integer.valueOf(args[1]);
				this.outputFile = new File(root.getParentFile(), String.format("main.%d.org.lywam.tours.phonegap.obb", versionCode));
				this.tempDirectory = Files.createTempDirectory(root.getParentFile().toPath(), "temp").toFile();
				getFilesRecursive(root);
				createZip();
				FileUtils.deleteDirectory(this.tempDirectory);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	private void getFilesRecursive(File parent)
	{
		for (File files : parent.listFiles())
		{
			if (files.isDirectory())
			{
				getFilesRecursive(files);
			}
			else
			{
				try
				{
					FileUtils.copyFileToDirectory(files, this.tempDirectory);
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	private void compressDirectoryToZipfile(String rootDir, String sourceDir, ZipOutputStream out) throws IOException, FileNotFoundException
	{
		for (File file : new File(sourceDir).listFiles())
		{
			if (file.isDirectory())
			{
				compressDirectoryToZipfile(rootDir, sourceDir + file.getName() + File.separator, out);
			}
			else
			{
				ZipEntry entry = new ZipEntry(file.getName());
				out.putNextEntry(entry);
				FileInputStream in = new FileInputStream(sourceDir + file.getName());
				IOUtils.copy(in, out);
				IOUtils.closeQuietly(in);
			}
		}
	}

	public void createZip()
	{
		try
		{
			ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(outputFile));
			compressDirectoryToZipfile(tempDirectory.getAbsolutePath(), tempDirectory.getAbsolutePath() + File.separator, zipFile);
			IOUtils.closeQuietly(zipFile);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		new ExpansionCreator(args);
	}
}